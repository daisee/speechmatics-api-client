{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module PeekJobSpec (spec) where

import           Data.ByteString.Lazy
import           Data.Text.Encoding                    (encodeUtf8)
import           NeatInterpolation                     (text)
import           Speechmatics.JSON.PeekJob
import qualified Speechmatics.JSON.PeekJob.ApplianceV1 as ApplianceV1
import qualified Speechmatics.JSON.PeekJob.CloudV2     as CloudV2
import           Test.Hspec

progressExpectedApplianceV1 :: Job
progressExpectedApplianceV1 = Job {
    jobCheckWait = Just 30
  , jobStatus = JobRunning
}

progressExpectedCloudV2 :: Job
progressExpectedCloudV2 = Job {
    jobCheckWait = Nothing
  , jobStatus = JobRunning
}

progressFixtureApplianceV1 :: ByteString
progressFixtureApplianceV1 = fromStrict $ encodeUtf8 [text|
{
  "job": {
    "check_wait": 30,
    "created_at": "Tue, 29 May 2018 05:43:46 GMT",
    "duration": 4,
    "id": 8221819,
    "job_status": "transcribing",
    "job_type": "transcription",
    "lang": "en-US",
    "meta": null,
    "name": "file.mp3",
    "next_check": 1527572657,
    "notification": "email",
    "transcription": null,
    "url": "/v1.0/user/42578/jobs/8221819/audio",
    "user_id": 42578
  }
}
|]

progressFixtureCloudV2 :: ByteString
progressFixtureCloudV2 = fromStrict $ encodeUtf8 [text|
{
  "job": {
    "config": {
      "transcription_config": {
        "additional_vocab": [
          {
            "content": "X.Y.Z.Z.Y."
          }
        ],
        "language": "en"
      },
      "type": "transcription"
    },
    "created_at": "2020-05-10T23:54:01.023Z",
    "data_name": "initialisms1.wav",
    "duration": 20,
    "id": "0cvnzxogqu",
    "status": "running"
  }
}
|]


doneExpected :: Job
doneExpected = Job {
    jobCheckWait = Nothing
  , jobStatus = JobDone
}
doneFixtureApplianceV1 :: ByteString
doneFixtureApplianceV1 = fromStrict $ encodeUtf8 [text|
{
  "job": {
    "check_wait": null,
    "created_at": "Tue, 29 May 2018 04:21:48 GMT", "duration": 0,
    "id": 8220130,
    "job_status": "done",
    "job_type": "transcription",
    "lang": "en-US",
    "meta": null,
    "name": "zero.wav",
    "next_check": 0,
    "notification": "email",
    "transcription": "zero.json",
    "url": "/v1.0/user/42578/jobs/8220130/audio",
    "user_id": 42578
  }
}
|]

doneFixtureCloudV2 :: ByteString
doneFixtureCloudV2 = fromStrict $ encodeUtf8 [text|
{
  "job": {
    "config": {
      "transcription_config": {
        "additional_vocab": [
          {
            "content": "X.Y.Z.Z.Y."
          }
        ],
        "language": "en"
      },
      "type": "transcription"
    },
    "created_at": "2020-05-10T23:54:01.023Z",
    "data_name": "initialisms1.wav",
    "duration": 20,
    "id": "0cvnzxogqu",
    "status": "done"
  }
}
|]

spec :: Spec
spec = do
  describe "ApplianceV1 stats output parser" $ do
    it "parses doneFixture to doneExpected value" $ do
      (parse ApplianceV1.toJob doneFixtureApplianceV1) `shouldBe` (Right doneExpected)
    it "parses progressFixture to doneExpected value" $ do
      (parse ApplianceV1.toJob progressFixtureApplianceV1) `shouldBe` (Right progressExpectedApplianceV1)
    it "does not parse invalid json" $ do
      (parse ApplianceV1.toJob "not json") `shouldSatisfy` (\case
                                             Left _ -> True
                                             _ -> False)

  describe "CloudV2 stats output parser" $ do
    it "parses doneFixture to doneExpected value" $ do
      (parse CloudV2.toJob doneFixtureCloudV2) `shouldBe` (Right doneExpected)
    it "parses progressFixture to doneExpected value" $ do
      (parse CloudV2.toJob progressFixtureCloudV2) `shouldBe` (Right progressExpectedCloudV2)
    it "does not parse invalid json" $ do
      (parse CloudV2.toJob "not json") `shouldSatisfy` (\case
                                             Left _ -> True
                                             _ -> False)
