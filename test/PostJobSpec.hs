{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module PostJobSpec (spec) where

import           Data.ByteString.Lazy
import           Data.Text.Encoding        (encodeUtf8)
import           NeatInterpolation         (text)
import           Speechmatics.JSON.PostJob
import           Test.Hspec

expected :: PostJob
expected = PostJob {
    checkWait = Nothing,
    postId = IntId 8217275
  }

fixture :: ByteString
fixture = fromStrict $ encodeUtf8 [text|
{
  "balance": 9195,
  "check_wait": null,
  "cost": 0,
  "id": 8217275
}|]

spec :: Spec
spec = do
  describe "Ceres stats output parser" $ do
    it "parses fixture to expected value" $ do
      (parse fixture) `shouldBe` (Right expected)
    it "does not parse invalid json" $ do
      (parse "not json") `shouldSatisfy` (\case
                                             Left _ -> True
                                             _ -> False)
