{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module ConfigurationSpec (spec) where

import           Data.Aeson                 (eitherDecode, encode)
import           Data.ByteString.Lazy       (ByteString, fromStrict)
import           Data.Char                  (isSpace)
import qualified Data.Text                  as T
import           Data.Text.Encoding         (encodeUtf8)
import           NeatInterpolation          (text)
import           Speechmatics.Configuration
import           Test.Hspec                 (Spec, describe, it, shouldBe)

configuration :: ByteString
configuration = fromStrict . encodeUtf8 $ strip
    [text|
        {
            "type": "transcription",
            "transcription_config": {
                "language": "en-AU",
                "additional_vocab": [
                    { "content": "heychar", "sounds_like": ["hr", "heyyaa"] },
                    "coke"
                ],
                "channel_diarization_labels": ["agent", "caller"],
                "diarization": "channel"
            }
        }
    |]
  where
    strip x = T.concat $ T.words =<< T.lines x

spec :: Spec
spec = describe "Configuration encoding and decoding" $ do
    it "decodes configuration fixture into expected value" $ do
        decodedConfiguration `shouldBe` Right jobConfig

    it "roundtrips a configuration fixture" $ do
        (eitherDecode =<< (encode <$> decodedConfiguration)) `shouldBe` Right jobConfig

  where
    jobConfig = JobConfig
        { jobType = Transcription
        , jobTranscriptionConfig = Just $ TranscriptionConfig
            { language                 = "en-AU"
            , channelDiarizationLabels = Just ["agent", "caller"]
            , diarization              = Just Channel
            , additionalVocab          = Just
                [ CustomDictionary $ WordDictionary
                    { content    = "heychar"
                    , soundsLike = ["hr", "heyyaa"]
                    }
                , CustomPhrase "coke"
                ]
            }
        }
    decodedConfiguration :: Either String JobConfig
    decodedConfiguration = eitherDecode configuration
