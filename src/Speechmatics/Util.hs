module Speechmatics.Util where

import           Text.Casing (Identifier (..), fromHumps, toQuietSnake,
                              toScreamingSnake)

type Parts = Identifier String

dropParts :: Int -> Parts -> Parts
dropParts c = Identifier . drop c . unIdentifier

snakeDropping :: (Identifier String -> String) -> Int -> String -> String
snakeDropping f c = f . dropParts c . fromHumps

quietSnakeDropping :: Int -> String -> String
quietSnakeDropping = snakeDropping toQuietSnake
