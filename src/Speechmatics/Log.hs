{-# LANGUAGE OverloadedStrings #-}

-- | Simple log wrapper
module Speechmatics.Log(
  info, debug, warning, error
) where

import           Data.Text
import           Data.Text.Format.Heavy.Instances (Single (..))
import qualified Data.Text.Lazy                   as Lazy
import           Prelude                          ()
import           System.Log.Heavy.LoggingT        (LoggingT (..))
import qualified System.Log.Heavy.Shortcuts       as Log

source :: Text
source = "Speechmatics client"
info d = Log.info (Lazy.fromStrict d) (Single source)
debug d = Log.debug (Lazy.fromStrict d) (Single source)
warning d = Log.warning (Lazy.fromStrict d) (Single source)
error d = Log.reportError (Lazy.fromStrict d) (Single source)
