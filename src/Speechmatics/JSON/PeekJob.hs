{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Speechmatics.JSON.PeekJob (
    (.:??)
  , Job (..)
  , JobStatus (..)
  , parse
  ) where

import           Control.Monad              (join, (<=<))
import           Data.Aeson                 (FromJSON (..), Value (..),
                                             eitherDecode, withObject, (.:),
                                             (.:?))
import qualified Data.Aeson.Types           as AT
import qualified Data.ByteString.Lazy.Char8 as BSL
import qualified Data.HashMap.Strict        as HM
import           Data.Text                  (Text)
import           GHC.Generics               (Generic)

-- | Workaround for https://github.com/bos/aeson/issues/287.
(.:??) :: FromJSON a => HM.HashMap Text Value -> Text -> AT.Parser (Maybe a)
o .:?? val = fmap join (o .:? val)

data JobStatus =
    JobRunning
  | JobDone
  deriving (Eq, Show)

data Job = Job {
    jobCheckWait :: Maybe Int
  , jobStatus    :: JobStatus
  } deriving (Show, Eq, Generic)

parseTopLevel :: (Value -> AT.Parser Job) -> Value -> AT.Parser Job
parseTopLevel p = withObject "Speechmatics.JSON.PeekJob.Job" $ \o -> p =<< (o .: "job")

parse :: (Value -> AT.Parser Job) -> BSL.ByteString -> Either String Job
parse p = AT.parseEither (parseTopLevel p) <=< eitherDecode
