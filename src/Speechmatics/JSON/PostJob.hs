{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- | Generated from json response, now it's type safe, see the tests
module Speechmatics.JSON.PostJob where

import           Control.Monad              (join, mzero)
import           Data.Aeson                 (FromJSON (..), Value (..),
                                             eitherDecode, (.:), (.:?))
import qualified Data.ByteString.Lazy.Char8 as BSL
import           Data.Scientific            (floatingOrInteger)
import           Data.Text                  (Text)
import qualified Data.Text                  as T
import           GHC.Generics               (Generic)

-- | Workaround for https://github.com/bos/aeson/issues/287.
o .:?? val = fmap join (o .:? val)

data JobId =
    IntId !Integer
  | TextId !Text
  deriving (Eq)

instance Show JobId where
  show (IntId n)  = show n
  show (TextId t) = T.unpack t

instance FromJSON JobId where
  parseJSON (Number n) = either (const mzero) (pure . IntId) . floatingOrInteger $ n
  parseJSON (String s) = pure . TextId $ s
  parseJSON _ = mzero

-- | This skeleton is shared by both cloud and appliance APIs (for now).
data PostJob = PostJob {
    checkWait :: Maybe Value,
    postId    :: JobId
  } deriving (Show, Eq, Generic)

instance FromJSON PostJob where
  parseJSON (Object v) = PostJob <$> v .:?? "check_wait" <*> v .:   "id"
  parseJSON _          = mzero

parse :: BSL.ByteString -> Either String PostJob
parse = eitherDecode
