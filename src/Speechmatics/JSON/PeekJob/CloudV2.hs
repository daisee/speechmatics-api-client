{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Speechmatics.JSON.PeekJob.CloudV2 (
    toJob
  ) where

import           Control.Monad             (mzero)
import           Data.Aeson                (Value (..), (.:))
import qualified Data.Aeson.Types          as AT
import           Speechmatics.JSON.PeekJob

toJob :: Value -> AT.Parser Job
toJob (Object o) =
  Job Nothing
    <$> (toJobStatus =<< (o .: "status"))
toJob _ = mzero

toJobStatus :: Value -> AT.Parser JobStatus
toJobStatus (String "running") = pure JobRunning
toJobStatus (String "done")    = pure JobDone
toJobStatus _                  = mzero
