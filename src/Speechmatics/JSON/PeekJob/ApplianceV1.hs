{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Speechmatics.JSON.PeekJob.ApplianceV1 (
    toJob
  ) where

import           Control.Monad             (mzero)
import           Data.Aeson                (Value (..), (.:))
import qualified Data.Aeson.Types          as AT
import           Speechmatics.JSON.PeekJob

toJob :: Value -> AT.Parser Job
toJob (Object o) =
  Job
    <$> (o .:?? "check_wait")
    <*> (toJobStatus =<< (o .: "job_status"))
toJob _ = mzero

toJobStatus :: Value -> AT.Parser JobStatus
toJobStatus (String "transcribing") = pure JobRunning
toJobStatus (String "done")         = pure JobDone
toJobStatus _                       = mzero
