{-# LANGUAGE OverloadedStrings #-}
module Speechmatics.Client(
  transcribeBytes,
  AuthToken,
  UserID,
  LoadedFile(..),
  Error(..),
  Format(..),
  ByteConfig(..),
  BaseUrl(..),
  API(..)
) where

import           Control.Applicative
import           Control.Concurrent           (threadDelay)
import           Control.Monad.Except
import           Data.Aeson                   (Value)

import           Control.Monad.Trans.Maybe    (MaybeT (..), runMaybeT)
import qualified Speechmatics.Client.AzureApi as AzureApi
import           Speechmatics.Client.Types
import qualified Speechmatics.Client.WebApi   as WebApi
import           Speechmatics.Configuration   (JobConfig)
import           System.Log.Heavy.LoggingT    (LoggingT (..))

-- | Transcribe a file that is already loaded in memory
--   Does not catch exeptions
transcribeBytes :: ByteConfig -> LoadedFile -> LoggingT IO (Either Error Value)
transcribeBytes config file = do
  mbTranscriber <- createTranscriber config
  case mbTranscriber of
    Nothing          -> pure $ Left UnknownAPI
    Just transcriber -> transcribe transcriber (jobConfig config) file

createTranscriber :: ByteConfig -> LoggingT IO (Maybe SpeechmaticsTranscriber)
createTranscriber config = runMaybeT $
  MaybeT (WebApi.createTranscriber config) <|> MaybeT (AzureApi.createTranscriber config)

transcribe :: SpeechmaticsTranscriber
  -> JobConfig
  -> LoadedFile
  -> LoggingT IO (Either Error Value)
transcribe (SpeechmaticsTranscriber send fetch cleanup) config file = do
  sent <- send config file
  case sent of
    Left err -> pure (Left err)
    Right a  -> do
      res <- poll (MaxAttempts 300) (WaitSeconds 30) (fetch a)
      cleanup a
      case res of
        TranscriptionStatusUnknown   -> pure $ Left WaitTimeoutExceeded
        TranscriptionInProgress _    -> pure $ Left WaitTimeoutExceeded
        TranscriptionFailed error    -> pure $ Left error
        TranscriptionSucceeded value -> pure $ Right value

poll :: MaxAttempts -> WaitSeconds -> LoggingT IO TranscriptionStatus -> LoggingT IO TranscriptionStatus
poll (MaxAttempts 0) _  _ = pure TranscriptionStatusUnknown
poll (MaxAttempts n) wait action = do
  waitFor wait
  res <- action
  case res of
    -- If the job doesn't report status, use the max attempt counter to eventually stop waiting
    TranscriptionStatusUnknown       -> poll (MaxAttempts (n-1)) wait action

    -- The process is definitely alive, but no ETA. Use previously known timeout as ETA.
    TranscriptionInProgress Nothing  -> poll (MaxAttempts n) wait action

    -- The job is alive and provides ETA. Wait for ETA before checking.
    TranscriptionInProgress (Just w) -> poll (MaxAttempts n) w action

    -- we are done here
    res'                             -> pure res'
  where
    waitFor (WaitSeconds x) = liftIO $ threadDelay (1000000*x)


