{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Speechmatics.Configuration where

import           Data.Aeson.TH     (SumEncoding (..), constructorTagModifier,
                                    defaultOptions, deriveJSON,
                                    fieldLabelModifier, omitNothingFields,
                                    sumEncoding, tagSingleConstructors)
import           Data.Text         (Text)
import           Speechmatics.Util (quietSnakeDropping)

data JobType = Transcription deriving (Eq, Show)

data Diarization
    = Speaker
    | Channel
    deriving (Eq, Show)

data AdditionalVocab
    = CustomPhrase Text
    | CustomDictionary WordDictionary
    deriving (Eq, Show)

data WordDictionary = WordDictionary
    { content    :: Text
    , soundsLike :: [Text]
    } deriving (Eq, Show)

data JobConfig = JobConfig
    { jobType                :: JobType
    , jobTranscriptionConfig :: Maybe TranscriptionConfig
    } deriving (Eq, Show)

data TranscriptionConfig = TranscriptionConfig
    { language                 :: Text
    , additionalVocab          :: Maybe [AdditionalVocab]
    , channelDiarizationLabels :: Maybe [Text]
    , diarization              :: Maybe Diarization
    } deriving (Eq, Show)

$(deriveJSON defaultOptions{ sumEncoding = UntaggedValue } ''AdditionalVocab)
$(deriveJSON defaultOptions{ fieldLabelModifier = quietSnakeDropping 1 } ''JobConfig)
$(deriveJSON defaultOptions{ fieldLabelModifier = quietSnakeDropping 0 } ''WordDictionary)
$(deriveJSON defaultOptions
    { fieldLabelModifier = quietSnakeDropping 0
    , omitNothingFields = True
    } ''TranscriptionConfig)
$(deriveJSON defaultOptions{ constructorTagModifier = quietSnakeDropping 0 } ''Diarization)
$(deriveJSON defaultOptions
    { constructorTagModifier = quietSnakeDropping 0
    , sumEncoding            = UntaggedValue
    , tagSingleConstructors  = True
    } ''JobType)
