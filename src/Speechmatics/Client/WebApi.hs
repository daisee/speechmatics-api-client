{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
module Speechmatics.Client.WebApi
where

import           Control.Lens
import           Control.Monad.Trans                   (lift)
import           Data.Aeson                            (Value, eitherDecode,
                                                        encode)
import           Data.Aeson.Lens                       (key, _String)
import           Data.Bifunctor                        (first)
import qualified Data.ByteString.Char8                 as C8BS
import qualified Data.ByteString.Lazy                  as LBS
import           Data.String                           (IsString)
import           Data.Text                             (Text)
import qualified Data.Text                             as Text
import           Data.Text.Encoding                    (encodeUtf8)
import           Data.UUID                             as UUID
import           Data.UUID.V4                          as UUID
import           Network.HTTP.Types.URI                (QueryText,
                                                        queryTextToQuery,
                                                        renderQuery)
import           Network.Wreq                          (Options, responseBody)
import qualified Network.Wreq                          as Wreq
import           Speechmatics.Client.Types
import           Speechmatics.Configuration            (JobConfig)
import           Speechmatics.JSON.PeekJob             (Job (..),
                                                        JobStatus (..))
import qualified Speechmatics.JSON.PeekJob             as Peek
import qualified Speechmatics.JSON.PeekJob.ApplianceV1 as ApplianceV1
import qualified Speechmatics.JSON.PeekJob.CloudV2     as CloudV2
import           Speechmatics.JSON.PostJob             (JobId)
import qualified Speechmatics.JSON.PostJob             as Post
import qualified Speechmatics.Log                      as Log
import qualified Speechmatics.Request                  as Sess
import           System.Log.Heavy.LoggingT             (LoggingT (..))

newtype JobsUrl   = JobsUrl { unJobsUrl :: String } deriving (Eq, Show, IsString)
newtype JobUrl    = JobUrl { unJobUrl :: String } deriving (Eq, Show, IsString)

data Endpoint = Endpoint {
  baseUrl     :: BaseUrl,
  options     :: Options,
  parseJob    :: LBS.ByteString -> Either String Job,

-- | Validate that the tag we set when submitting the job is present in the
--   result. This ensures it is in fact the job we submitted, and not merely
--   a job with the same ID.
  validateTag :: Text -> Value -> Either Error Value
}

createTranscriber :: ByteConfig -> LoggingT IO (Maybe SpeechmaticsTranscriber)
createTranscriber config = do
  let currentApi = api config
  let endpoint = case currentApi of
        ApplianceV1 user base -> Just $ applianceV1Endpoint base user
        CloudV2 tok base      -> Just $ cloudV2Endpoint base tok
        AzureStorage _ _ _    -> Nothing

  pure $ fmap buildTranscriber endpoint
  where
    buildTranscriber endpoint = SpeechmaticsTranscriber
      { sendJob      = sendTranscription endpoint
      , fetchResults = fetchTranscription endpoint (format config)
      , cleanup      = const (pure ())
      }

sendTranscription :: Endpoint -> JobConfig -> LoadedFile -> LoggingT IO (Either Error (Sess.Session, JobId, Text))
sendTranscription (Endpoint url opts _ _) config file = do
  tag <- UUID.toText <$> lift UUID.nextRandom
  let parts = fileToParts config file
  let tagged_parts = Wreq.partText "meta" tag : parts

  session  <- lift Sess.newSession

  Log.debug $ "Sending job [Tag: " <> tag <>"]"
  response <- Sess.postWith opts session (unJobsUrl $ postJobUrl url) tagged_parts

  case Post.parse (response ^. responseBody) of
    Left message -> return $ Left $ ParseError message
    Right parsed -> do
      let jobId  = Post.postId parsed
      let jobUrl = jobStatusUrl url jobId
      Log.debug $ "Job in progress [Tag: " <> tag <> ", ID: " <> Text.pack (show jobId) <> "]"
      pure $ Right (session, jobId, tag)

fetchTranscription :: Endpoint -> Format -> (Sess.Session, JobId, Text) -> LoggingT IO TranscriptionStatus
fetchTranscription (Endpoint url opts peekJob validateTag) format (session, jobId, tag) = do
  let logJobMarker = "Tag: " <> tag <> ", ID: " <> Text.pack (show jobId)
  let statusUrl = unJobUrl $ jobStatusUrl url jobId

  Log.debug $ "Fetching results [" <> logJobMarker <> "]"
  statusResponse <- Sess.getWith opts session statusUrl
  let job = peekJob $ statusResponse ^. responseBody
  case job of
    Left error -> pure . TranscriptionFailed . ParseError $ error
    Right (Job wait JobRunning) -> pure . TranscriptionInProgress $ fmap WaitSeconds wait
    Right (Job _ JobDone) -> do
      let transcriptUri = statusUrl <> "transcript" <> printQuery (formatQuery format)
      result <- Sess.getWith opts session transcriptUri
      let parsed = first ParseError $ eitherDecode (result ^. responseBody)
      case parsed >>= validateTag tag of
        Left error  -> do
          Log.error $ "Job failed [" <> logJobMarker <> "]: " <> Text.pack (show error)
          pure $ TranscriptionFailed error
        Right value -> do
          Log.debug $ "Job succeeded [" <> logJobMarker <> "]"
          pure $ TranscriptionSucceeded value

fileToParts :: JobConfig -> LoadedFile -> [Wreq.Part]
fileToParts config (LoadedFile content filename mimetype) =
  [
    Wreq.partLBS "data_file" content
      & Wreq.partContentType ?~ mimetype
      & Wreq.partFileName ?~ filename,
    Wreq.partLBS "config" $ encode config
  ]

applianceV1Endpoint :: BaseUrl -> UserID -> Endpoint
applianceV1Endpoint (BaseUrl base) user = Endpoint
  { baseUrl     = BaseUrl $ concat [base, show user, "/"]
  -- appliance API does not support authentication, but requires a user fragment in the URI
  , options     = Wreq.defaults
  , parseJob    = Peek.parse ApplianceV1.toJob
  , validateTag = ensureTag
  }
  where
    ensureTag expected value =
      case value ^? key "job" . key "meta" . _String of
        Just t | t == expected -> Right value
        wrong                  -> Left $ TokenMismatch expected wrong

cloudV2Endpoint :: BaseUrl -> AuthToken -> Endpoint
cloudV2Endpoint base tok = Endpoint
  { baseUrl     = base
  -- cloud API requires a bearer token present in the Authorization header
  , options     = Wreq.defaults & Wreq.auth ?~ Wreq.oauth2Bearer (encodeUtf8 tok)
  , parseJob    = Peek.parse CloudV2.toJob
  -- cloud API does not support echoing the meta tag;
  , validateTag = \_ v -> pure v
  }

formatQuery :: Format -> QueryText
formatQuery JsonV1 = []
formatQuery JsonV2 = [("format", Just "json-v2")]

postJobUrl :: BaseUrl -> JobsUrl
postJobUrl (BaseUrl url) =
  JobsUrl (url <> "jobs/")

jobStatusUrl :: BaseUrl -> JobId -> JobUrl
jobStatusUrl (BaseUrl url) jobId =
  JobUrl (url <> "jobs/" <> show jobId <> "/")

printQuery :: QueryText -> String
printQuery = C8BS.unpack . renderQuery True . queryTextToQuery
