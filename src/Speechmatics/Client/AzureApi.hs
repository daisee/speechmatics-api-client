{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeApplications           #-}
module Speechmatics.Client.AzureApi
where

import qualified Azure.Storage.Blob         as Azure
import           Control.Exception          (IOException, try)
import           Control.Monad.Trans        (lift)
import           Data.Aeson                 (Value, eitherDecode, encode)
import qualified Data.Text                  as Text
import           Data.UUID                  (UUID)
import qualified Data.UUID                  as UUID
import qualified Data.UUID.V4               as UUID
import           Network.HTTP.Client        (HttpException)
import           Speechmatics.Client.Types
import           Speechmatics.Configuration (JobConfig)
import qualified Speechmatics.Log           as Log
import           System.Log.Heavy.LoggingT  (LoggingT (..))

data Endpoint = Endpoint {
  storageAccount   :: Azure.Account,
  inputsContainer  :: Azure.ContainerName,
  resultsContainer :: Azure.ContainerName
}

createTranscriber :: ByteConfig -> LoggingT IO (Maybe SpeechmaticsTranscriber)
createTranscriber config = do
  let currentApi = api config
  let endpoint = case currentApi of
        AzureStorage acc inputs results ->
          Just $ Endpoint
                  { storageAccount   = acc
                  , inputsContainer  = inputs
                  , resultsContainer = results
                  }
        ApplianceV1 _ _ -> Nothing
        CloudV2 _ _     -> Nothing

  pure $ fmap buildTranscriber endpoint
  where
    buildTranscriber endpoint = SpeechmaticsTranscriber
      { sendJob      = sendTranscription endpoint
      , fetchResults = fetchTranscription endpoint (format config)
      , cleanup      = cleanupStorage endpoint
      }

sendTranscription :: Endpoint
  -> JobConfig
  -> LoadedFile
  -> LoggingT IO (Either Error UUID.UUID)
sendTranscription endpoint config (LoadedFile content _ mimetype) = do
  jobId <- lift UUID.nextRandom

  Log.debug $ "Sending job [ID: " <> UUID.toText jobId <>"]"
  lift $ putBlob (configBlobName jobId) (encode config)
  lift $ putBlob (audioBlobName jobId) content
  Log.debug $ "Job in progress [ID: " <> UUID.toText jobId <>"]"

  pure $ Right jobId
  where
    putBlob blobName payload =
      Azure.putBlobStream (storageAccount endpoint) (inputsContainer endpoint) blobName payload

fetchTranscription :: Endpoint
  -> Format
  -> UUID.UUID
  -> LoggingT IO TranscriptionStatus
fetchTranscription endpoint format jobId = do
  Log.debug $ "Fetching results [ID: " <> UUID.toText jobId <>"]"
  res <- lift $ Azure.getBlobBS (storageAccount endpoint) (resultsContainer endpoint) (transcriptionBlobName jobId)
  case res of
    Nothing -> pure TranscriptionStatusUnknown
    Just bs -> do
      case eitherDecode bs of
        Left error  -> do
          Log.debug $ "Job failed [ID: " <> UUID.toText jobId <>"]: " <> Text.pack (show error)
          pure . TranscriptionFailed . ParseError $ error
        Right value -> do
          Log.debug $ "Job succeeded [ID: " <> UUID.toText jobId <>"]"
          pure $ TranscriptionSucceeded value

cleanupStorage :: Endpoint -> UUID.UUID -> LoggingT IO ()
cleanupStorage endpoint jobId = do
  Log.debug $ "Clean up [ID: " <> UUID.toText jobId <>"]"
  res <- lift . try @HttpException $ do
    Azure.deleteBlob (storageAccount endpoint) (resultsContainer endpoint) (transcriptionBlobName jobId)
    Azure.deleteBlob (storageAccount endpoint) (inputsContainer endpoint) (audioBlobName jobId)
    Azure.deleteBlob (storageAccount endpoint) (inputsContainer endpoint) (configBlobName jobId)
  case res of
    Right _ -> pure ()
    Left err ->
      Log.warning $ "Job cleanup failed. Will ignore this error and continue: " <> Text.pack (show err)


audioBlobName :: UUID.UUID -> Azure.BlobName
audioBlobName uuid = Azure.BlobName $ UUID.toText uuid <> ".audio"

configBlobName :: UUID.UUID -> Azure.BlobName
configBlobName uuid = Azure.BlobName $ UUID.toText uuid <> ".config"

transcriptionBlobName :: UUID.UUID -> Azure.BlobName
transcriptionBlobName uuid = Azure.BlobName $ UUID.toText uuid <> ".transcription"
