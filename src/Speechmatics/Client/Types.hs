{-# LANGUAGE ExistentialQuantification  #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Speechmatics.Client.Types
where

import qualified Azure.Storage.Blob         as Azure
import           Data.Aeson                 (Value)
import qualified Data.ByteString.Lazy       as LBS
import           Data.String                (IsString)
import           Data.Text                  (Text)
import           Network.Mime               (MimeType)
import           Speechmatics.Configuration (JobConfig)
import           System.Log.Heavy.LoggingT  (LoggingT (..))

-- | Authorization token obtained from speechmatics
type AuthToken = Text
-- | UserID obtained from speechmatics
type UserID = Integer

newtype BaseUrl   = BaseUrl { unBaseUrl :: String } deriving (Eq, Show, IsString)

newtype WaitSeconds = WaitSeconds Int deriving (Show, Eq, Ord)
newtype MaxAttempts = MaxAttempts Int deriving (Show, Eq, Ord)

-- | Status of the transctiption
data TranscriptionStatus
  = TranscriptionFailed Error                   -- ^ Transcription failed with the specified error
  | TranscriptionSucceeded Value                -- ^ Transcription succeeded with the specified value
  | TranscriptionInProgress (Maybe WaitSeconds) -- ^ Transcribing is in progress, check in the specified amount of seconds

  -- | No transcription status. This constructor does not represent an error.
  -- Some transcribers don't report the status in progress and the transcription results
  -- should be monitored in a check/retry loop.
  | TranscriptionStatusUnknown
  deriving (Eq, Show)

data LoadedFile = LoadedFile {
  content  :: LBS.ByteString,
  filename :: String,
  mimetype :: MimeType -- to get this Jappie used defaultMimeLookup "f.mp3"
}

data Error
  = UnkownResponse
  | UnknownAPI
  | MetaPartSet
  | ParseError String
  | WaitTimeoutExceeded
  | TokenMismatch { expected :: Text, got :: Maybe Text }
  deriving(Eq, Show)

data Format = JsonV1 | JsonV2 deriving (Eq, Show)

data API
  = ApplianceV1 !UserID !BaseUrl
  | CloudV2 !AuthToken !BaseUrl
  | AzureStorage !Azure.Account !Azure.ContainerName !Azure.ContainerName
  deriving (Eq, Show)

-- | Configuration data for transcribeBytes function
data ByteConfig = ByteConfig {
  api       :: API,
  format    :: Format,
  jobConfig :: JobConfig
} deriving (Eq, Show)

-- | A transcriber consists of two parts:
--   sending the job and fetching the result.
--
-- A hidden intermediate type allows data exchange between
-- sending and fetching functions
data SpeechmaticsTranscriber
  = forall a. SpeechmaticsTranscriber
                { sendJob        :: JobConfig -> LoadedFile -> LoggingT IO (Either Error a)
                , fetchResults   :: a -> LoggingT IO TranscriptionStatus
                , cleanup        :: a -> LoggingT IO ()
                }
