# speechmatics

a client for the [speechmatics](https://app.speechmatics.com/api-details) api .

with input an audio file and blocking untill we get a json transcription.


Developed by [Daisee](https://www.daisee.com/), pull requests are accepted:
https://bitbucket.org/daisee/speechmatics-api-client
