module Main where

import           Data.Aeson                 (eitherDecodeFileStrict)
import           Options.Applicative
import           Speechmatics.Configuration (JobConfig)

parser :: Parser FilePath
parser = argument str (metavar "FILE")

readSettings :: IO FilePath
readSettings = customExecParser (prefs showHelpOnError) $ info
    (helper <*> parser)
    (fullDesc <> Options.Applicative.header "Speechmatics config validation"
              <> progDesc "Validates a speechmatics configuration file")

speechmaticsConfig :: FilePath -> IO JobConfig
speechmaticsConfig fp = either error pure =<< eitherDecodeFileStrict fp

main :: IO ()
main = do
    fp <- readSettings
    _config <- speechmaticsConfig fp
    pure ()
