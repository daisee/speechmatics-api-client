{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Azure.Storage.Blob        as Azure
import           Data.Aeson                (decode)
import qualified Data.ByteString.Lazy      as LBS
import           Data.Digest.Pure.SHA
import           Data.Maybe
import           Data.String               (fromString)
import           Data.Text                 (Text, pack)
import           Network.Mime              (MimeType, defaultMimeLookup)
import           Options
import           Speechmatics.Client
import           System.Log.Heavy
import           System.Log.Heavy.Backends (defStdoutSettings)
import           System.Log.Heavy.Types    (LoggingSettings (..))

data MainOptions = MainOptions
  { targetFile :: Maybe FilePath
  }

instance Options MainOptions where
    defineOptions = pure MainOptions
      <*> simpleOption "file" Nothing "The file to be posted"

data CloudOptions = CloudOptions
  { token    :: Maybe String
  , cloudUrl :: BaseUrl
  }

instance Options CloudOptions where
  defineOptions = pure CloudOptions
    <*> simpleOption "token" Nothing
        "The bearer token obtained from speechmatics"
    <*> (fromString <$> simpleOption "uri" "https://trial.asr.api.speechmatics.com/v2/" "Speechmatics Endpoint")

data AzureOptions = AzureOptions
  { accountName :: Azure.AccountName
  , sas         :: Maybe Azure.SAS
  , inputs      :: Azure.ContainerName
  , results     :: Azure.ContainerName
  }

instance Options AzureOptions where
  defineOptions = pure AzureOptions
    <*> (fromString <$> simpleOption "account-name" "transcribersstorage" "Azure Storage Account name")
    <*> (fmap fromString <$> simpleOption "sas" Nothing "Azure Storage SAS")
    <*> (fromString <$> simpleOption "inputs-container" "speechmatics-inputs" "Container name for audio files to go in")
    <*> (fromString <$> simpleOption "results-container" "speechmatics-results" "Container name to fetch the results from")

runCloud :: MainOptions -> CloudOptions -> [String] -> IO ()
runCloud mainOpts opts _ = do
  case (token opts, targetFile mainOpts) of
    (Nothing, Nothing)    -> print "Missing required arguments: --token, --file"
    (Nothing, _)          -> print "Missing required arguments: --token"
    (_, Nothing)          -> print "Missing required arguments: --file"
    (Just tok, Just file) ->
      transcribeFile file $ ByteConfig
        { api       = CloudV2 (pack tok) (cloudUrl opts)
        , format    = JsonV2
        , jobConfig = speechmaticsConfig
        }

runAzure :: MainOptions -> AzureOptions -> [String] -> IO ()
runAzure mainOpts opts _ =
  case (sas opts, targetFile mainOpts) of
    (Nothing, Nothing)    -> print "Missing required arguments: --sas, --file"
    (Nothing, _)          -> print "Missing required arguments: --sas"
    (_, Nothing)          -> print "Missing required arguments: --file"
    (Just sas, Just file) -> do
      let account = Azure.Account (accountName opts) sas
      transcribeFile file $ ByteConfig
        { api       = AzureStorage account (inputs opts) (results opts)
        , format    = JsonV2
        , jobConfig = speechmaticsConfig
        }

main :: IO ()
main = runSubcommand
  [ subcommand "cloud" runCloud
  , subcommand "azure" runAzure
  ]

runStdoutLoggingT = withLoggingT $ LoggingSettings defStdoutSettings

speechmaticsConfig = fromJust $ decode "{ \"type\": \"transcription\", \"transcription_config\": { \"language\": \"en\", \"diarization\": \"channel\", \"channel_diarization_labels\": [\"Agent\", \"Caller\"] } }"

transcribeFile :: FilePath -> ByteConfig -> IO ()
transcribeFile file byteConfig = do
  bytes <- LBS.readFile file
  let mime = (defaultMimeLookup "d.mp3")
  print mime
  print . sha256 $ bytes

  result <- runStdoutLoggingT $ transcribeBytes byteConfig (LoadedFile bytes file mime)
  putStrLn . show $ result
